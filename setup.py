from setuptools import setup, find_packages

setup(
    name="cypher",
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        "Flask==0.12.2",
        "WTForms==2.1",
        "Flask-Babel==0.11.2",
        "Flask-WTF==0.14.2",
        "pymongo==3.11.0",
        "dnspython==2.0.0",
        "pycryptodome==3.4.6",
        "pytest==3.3.1",
        "raven==6.1.0",
        "blinker==1.4",
        "gunicorn==19.7.1"
    ],
    setup_requires=[
        "pytest-runner",
    ],
    tests_require=[
        "pytest",
    ],
)
