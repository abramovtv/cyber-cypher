import re
import wtforms

from flask_wtf import FlaskForm
from wtforms.validators import DataRequired, Regexp, NumberRange, Length, Optional, ValidationError
from wtforms.widgets import Input
from cypher import app


class DataForm(FlaskForm):
    CHOICE_TEXT = "text"
    CHOICE_FILE = "file"
    TIME_PATTERN = r"^(\d{1,2}[dhm]\s?){1,3}$"
    TIME_MAP = {"d": 86400, "h": 3600, "m": 60}

    time = wtforms.StringField(
        "Время жизни",
        description="1d2h3m = 1 день 2 часа и 3 минуты. Не более 3х дней",
        validators=[DataRequired(), Regexp(TIME_PATTERN)],
        default="30m",
        render_kw={"pattern": TIME_PATTERN}
    )

    count = wtforms.IntegerField(
        "Максимум открытий",
        validators=[DataRequired(), NumberRange(1, app.config['MAX_TIMES'])],
        default=1,
        widget=Input(input_type="number"),
        render_kw={"min": 1, "max": app.config['MAX_TIMES']}
    )

    type_select = wtforms.RadioField(
        "Выбор",
        choices=[(CHOICE_TEXT, "Текст"), (CHOICE_FILE, "Файл")],
        validators=[DataRequired()],
        default=CHOICE_TEXT
    )

    text = wtforms.TextAreaField(
        "Секретный текст",
        description="не более 64Kb",
        validators=[Length(max=app.config['MAX_TEXT_LENGTH'])],
        render_kw={"maxlength": app.config['MAX_TEXT_LENGTH']}
    )

    file = wtforms.FileField(
        "Файл",
        description="не более 5Мб",
    )

    # Вычисляемые поля. Не принимаются с формы, а вычисляются из других полей
    lifetime = wtforms.IntegerField()  # Время жизни ссылки в секундах
    content = wtforms.StringField()  # Строка байтов полученная из текста или файла пользователя

    def validate_text(self, field):
        if self.type_select.data == self.CHOICE_TEXT:
            DataRequired()(self, field)
            self.content.data = bytes(field.data, encoding="utf-8")

    def validate_file(self, field):
        if self.type_select.data == self.CHOICE_FILE:
            DataRequired()(self, field)
            self.content.data = field.data.read(app.config['MAX_MSG_LENGTH'] + 1)
            if len(self.content.data) > app.config['MAX_MSG_LENGTH']:
                raise ValidationError("Допустимый размер не более {} Mb".format(
                    self.__format(app.config['MAX_MSG_LENGTH'] / 1048576)
                ))

    def validate_time(self, field):
        groups = re.findall("(\d+)([dhm])", field.data or "")
        lifetime = sum(int(g[0]) * self.TIME_MAP[g[1]] for g in groups) if groups else 0

        self.lifetime.data = lifetime
        if not 0 < lifetime <= app.config['MAX_EXPIRE_TIME']:
            raise ValidationError("Время жизни не более {} дн.".format(
                self.__format(app.config['MAX_EXPIRE_TIME'] / 86400)
            ))

    def __format(self, n):
        return "{0:.{1}f}".format(n, 0 if n == int(n) else 2)
