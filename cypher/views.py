import io

from werkzeug.exceptions import NotFound
from cypher import app
from datetime import datetime
from flask import (
    render_template, send_from_directory, jsonify,
    url_for, send_file, request
)
from cypher.storage import store, restore
from cypher.forms import DataForm


@app.route("/")
def index():
    form = DataForm()
    return render_template("index.html", form=form)


@app.route("/encode", methods=("POST",))
def encode():
    form = DataForm()
    if form.validate():
        data = form.data
        key = store(
            msg=data["content"],
            expire=data["lifetime"],
            times=data["count"],
            file_name=data["file"] and data["file"].filename
        )
        url = url_for("decode", hex_key=key, _external=True)
        return jsonify(ok=True, url=url)
    else:
        return jsonify(ok=False, errors=form.errors)


@app.route("/get/<hex_key>", methods=("GET", "POST"))
def decode(hex_key):
    if request.method != "POST":
        return render_template("bots.html")
    try:
        data, name = restore(hex_key)
    except ValueError:
        raise NotFound()

    if name:
        return send_file(io.BytesIO(data), as_attachment=True, attachment_filename=name)
    else:
        return render_template("message.html", data=data.decode("utf-8"))


@app.errorhandler(404)
def not_found(error):
    return render_template("error.html"), 404


@app.route("/static/<path:path>")
def serve_static(path):
    return send_from_directory("static", path)


@app.context_processor
def context_processor():
    """Наподобие django"вского тега {% now %}"""
    def now(fmt):
        return datetime.now().strftime(fmt)

    return {"now": now}
