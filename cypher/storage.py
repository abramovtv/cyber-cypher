# -*- coding: utf-8 -*-
import pymongo
from cypher import app, crypto
from bson.objectid import ObjectId
from datetime import datetime, timedelta

KEY_ID = "_id"
KEY_EXPIRE = "e"
KEY_TIMES = "t"
KEY_CODE = "c"
KEY_FILE = "f"

client = pymongo.MongoClient(app.config["MONGO_URL"])
db = client.get_database()
db.keys.create_index([(KEY_EXPIRE, pymongo.ASCENDING)], expireAfterSeconds=0)


def store(msg, expire=1800, times=1, file_name=""):
    """
    Зашифровать и сохранить данные и получить ключ для доступа к ним. Данные будут сохранены в mongodb
    :param msg: bytes - Данные для сохранения
    :param expire: int - Время жизни в секундах. Потом данные станут недоступны
    :param times: int - Максимальное количество извлечений данных. Потом данные станут недоступны
    :param file_name: string - Имя файла, если данные получены из файла
    :return bytes - ключ доступа к данным
    :raises ValueError - данные невозможно сохранить
    """
    if len(msg) > app.config['MAX_MSG_LENGTH']:
        raise ValueError("Message too long")

    cypher_key = crypto.key(app.config["AES_KEY_LENGTH"])
    code = crypto.encrypt(msg, cypher_key)

    data = {
        KEY_EXPIRE: datetime.utcnow() + timedelta(seconds=expire),
        KEY_TIMES: times,
        KEY_FILE: file_name,
        KEY_CODE: code
    }
    storage_key = db.keys.insert_one(data).inserted_id
    return str(storage_key) + cypher_key.hex()


def restore(hex_key):
    """
    Расшифровать и получить данные по ключу. Данные будут прочитаны из mongodb,
    если при сохранении данных передавалось имя файла, то будет возвращено и оно.
    :param hex_key: bytes - ключ доступа к данным
    :return (bytes, string) - словарь из расшифрованных данных и имени файла
    :raises ValueError - по ключу нельзя получить данные
    """
    try:
        key = bytes.fromhex(hex_key)  # Проверить что это строка hex
    except ValueError:
        raise ValueError("Not a HEX string")

    if len(key) != app.config["STORAGE_KEY_LENGTH"] + app.config["AES_KEY_LENGTH"]:
        raise ValueError("Invalid key length")

    storage_key = key[:app.config["STORAGE_KEY_LENGTH"]].hex()
    cypher_key = key[app.config["STORAGE_KEY_LENGTH"]:]

    data = db.keys.find_one_and_update(
        {KEY_ID: ObjectId(storage_key)},
        {"$inc": {KEY_TIMES: -1}}
    )

    if not data:
        raise ValueError("Key does not exist")

    if data[KEY_TIMES] <= 1:
        db.keys.delete_one({KEY_ID: ObjectId(storage_key)})

    msg = crypto.decrypt(cypher_key, data[KEY_CODE])

    return msg, data[KEY_FILE]
