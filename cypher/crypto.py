from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes

IV_LENGTH = 16  # длина инициализирующего вектора 16 байт


def encrypt(msg, key):
    """
    Зашифровать данные протоколом AES
    :param msg: bytes - Данные для зашифровки
    :param key: bytes - Ключ шифрования 16, 24 или 32 байта
    :return: bytes - Зашифрованные данные
    """
    cipher = AES.new(key, AES.MODE_CFB)
    code = cipher.iv + cipher.encrypt(msg)  # Вектор инициализации (16 байт) добавляем к сообщению
    return code


def decrypt(key, code):
    """
    Расшифровать данные
    :param key: bytes - Ключ шифрования 16, 24 или 32 байта
    :param code: bytes - Зашифрованные данные
    :return: bytes - Расшифрованные данные
    """
    cipher = AES.new(key, AES.MODE_CFB, iv=code[:IV_LENGTH])
    return cipher.decrypt(code[IV_LENGTH:])


def key(length=32):
    """
    Получить случайный ключ
    :param length: int - Длина ключа (по умолчанию 32 байта)
    :return: bytes - ключ
    """
    return get_random_bytes(length)
