from flask import Flask
from raven.contrib.flask import Sentry
from flask_babel import Babel
from flask_wtf.csrf import CSRFProtect

app = Flask(__name__)

# Читаем настройки приложения
app.config.from_object("settings")
app.config.from_envvar("SETTINGS", silent=True)

if "SENTRY_CONFIG" in app.config:
    sentry = Sentry(app)

babel = Babel(app)
csrf = CSRFProtect(app)

import cypher.views
