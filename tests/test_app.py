import json
import unittest
from cypher import app
from unittest.mock import patch
from io import BytesIO


class CypherTestCaseBase(unittest.TestCase):
    INDEX_URL = "/"
    ENCODE_URL = "/encode"
    DECODE_URL = "/get/deadbeef"

    def setUp(self):
        app.testing = True
        self.app = app.test_client()


class CypherTestCase(CypherTestCaseBase):
    def test_index_form_csrf(self):
        """
        Форма на главной странице содержит CSRF токен
        """
        rv = self.app.get(self.INDEX_URL)
        self.assertEqual(rv.status_code, 200)
        self.assertIn(b"<form", rv.data)
        self.assertIn(b"csrf_token", rv.data)

    def test_encode_get_error(self):
        """
        На ENCODE_URL нельзя обратиться с помощью GET
        """
        rv = self.app.get(self.ENCODE_URL)
        self.assertEqual(rv.status_code, 405)

    def test_encode_no_csrf_error(self):
        """
        На ENCODE_URL нельзя обратиться без CSRF токена
        """
        rv = self.app.post(self.ENCODE_URL)
        self.assertEqual(rv.status_code, 400)

    def test_encode_no_csrf_error(self):
        """
        На DECODE_URL нельзя обратиться через POST без CSRF токена
        """
        rv = self.app.post(self.DECODE_URL)
        self.assertEqual(rv.status_code, 400)


class CypherTestCaseNoCSRF(CypherTestCaseBase):
    def setUp(self):
        """
        Запрет проверки CSRF
        """
        app.config["WTF_CSRF_ENABLED"] = False
        return super().setUp()

    def test_encode_no_csrf_ok(self):
        """
        На ENCODE_URL можно обратиться без CSRF токена (только для тестов)
        """
        rv = self.app.post(self.ENCODE_URL)
        self.assertEqual(rv.status_code, 200)

    def test_no_data(self):
        """
        Если не предоставить обязательные данные, будут ошибки
        """
        rv = self.app.post(self.ENCODE_URL, data=dict(
            time="", count="", text="", type_select=""
        ))
        data = json.loads(rv.data)
        self.assertFalse(data["ok"])
        self.assertIn("count", data["errors"])
        self.assertIn("time", data["errors"])
        self.assertIn("type_select", data["errors"])

    def test_wrong_data(self):
        """
        Если предоставить некорректные данные, будут ошибки
        """
        rv = self.app.post(self.ENCODE_URL, data=dict(
            time="30d", count="9999", type_select="file"
        ))
        data = json.loads(rv.data)
        self.assertFalse(data["ok"])
        self.assertIn("count", data["errors"])
        self.assertIn("time", data["errors"])
        self.assertIn("file", data["errors"])

    @patch("cypher.views.store")
    def test_encode_text(self, store):
        """
        Если предоставить данные для кодирования текста, он закодируется
        и вернется Url для его декодирования
        """
        store.return_value = "deadbeef"
        rv = self.app.post(self.ENCODE_URL, data=dict(
            time="1m", count="1", text="test_string", type_select="text"
        ))
        data = json.loads(rv.data)
        store.assert_called()
        self.assertTrue(data["ok"])
        self.assertIn("url", data)

    @patch("cypher.views.store")
    def test_encode_file(self, store):
        """
        Если предоставить данные для кодирования файла, он закодируется
        и вернется Url для его декодирования
        """
        store.return_value = "deadbeef"
        rv = self.app.post(self.ENCODE_URL, data=dict(
            time="1m", count="1", type_select="file",
            file=(BytesIO(b"test_string"), "test.txt")
        ))
        data = json.loads(rv.data)
        store.assert_called()
        self.assertTrue(data["ok"])
        self.assertIn("url", data)

    @patch("cypher.views.store")
    def test_encode_file_error(self, store):
        """
        Если предоставить данные для кодирования файла, он закодируется
        и вернется Url для его декодирования
        """
        store.return_value = "deadbeef"
        rv = self.app.post(self.ENCODE_URL, data=dict(
            time="1m", count="1", type_select="file",
            file=(
                BytesIO(b"1" * (1 + app.config['MAX_MSG_LENGTH'])),
                "test.txt"
            )
        ))
        data = json.loads(rv.data)
        self.assertFalse(data["ok"])
        self.assertIn("file", data["errors"])

    @patch("cypher.views.restore")
    def test_decode_text(self, restore):
        """
        DECODE_URL возвращает страничку с данными, если был закодирован текст
        """
        restore.return_value = b"test_string", ""
        rv = self.app.post(self.DECODE_URL)
        self.assertEqual(rv.status_code, 200)
        self.assertNotIn("attachment", str(rv.headers))
        self.assertIn(restore.return_value[0], rv.data)

    @patch("cypher.views.restore")
    def test_decode_file(self, restore):
        """
        DECODE_URL возвращает файл для скачивания, если был закодирован файл
        """
        restore.return_value = b"test_string", "test.txt"
        rv = self.app.post(self.DECODE_URL)
        self.assertEqual(rv.status_code, 200)
        self.assertIn("attachment", str(rv.headers))
        self.assertEqual(restore.return_value[0], rv.data)

    @patch("cypher.views.restore")
    def test_decode_error(self, restore):
        """
        DECODE_URL возвращает 404, если уникальная ссылка устарела или не существует
        """
        restore.side_effect = ValueError()
        rv = self.app.post(self.DECODE_URL)
        self.assertEqual(rv.status_code, 404)

    @patch("cypher.views.restore")
    def test_decode_get(self, restore):
        """
        DECODE_URL возвращает заглушку, если обратиться через GET и до декодирования не доходит
        """
        restore.side_effect = ValueError()
        rv = self.app.get(self.DECODE_URL)
        self.assertEqual(rv.status_code, 200)
        self.assertIn(b'Javascript', rv.data)
