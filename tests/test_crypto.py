import unittest
from cypher.crypto import key, encrypt, decrypt


class CypherTestCase(unittest.TestCase):
    MESSAGE = b"test message"

    def test_key_len(self):
        """
        Ключ получается действительно запрошенной длины
        """
        key16 = key(16)
        key32 = key(32)
        self.assertEqual(len(key16), 16)
        self.assertEqual(len(key32), 32)

    def test_random_key(self):
        """
        Ключи получаются разные
        """
        key1 = key(32)
        key2 = key(32)
        self.assertNotEqual(key1, key2)

    def test_encrypted_different(self):
        """
        Зашифрованное сообщение каждый раз разное, даже с одним ключом
        """
        key16 = key(16)
        code1 = encrypt(self.MESSAGE, key16)
        code2 = encrypt(self.MESSAGE, key16)
        self.assertNotEqual(code1, code2)

    def test_encrypt_decrypt(self):
        """
        Обратно расшифровывается то же самое сообщение
        """
        key32 = key(32)
        code = encrypt(self.MESSAGE, key32)
        mess = decrypt(key32, code)
        self.assertEqual(mess, self.MESSAGE)
