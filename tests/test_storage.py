import unittest
from unittest.mock import patch
import cypher.storage as storage
from cypher import app


class CypherTestCase(unittest.TestCase):
    HEX = ""
    MSG = b"test"

    def setUp(self):
        self.HEX = "0f" * app.config["STORAGE_KEY_LENGTH"]

    @patch("cypher.storage.db")
    def test_store(self, db):
        """
        Сообщение сохраняется в mongo
        """

        db.keys.insert_one().inserted_id = self.HEX

        key = storage.store(self.MSG, expire=10, times=5)
        self.assertTrue(db.keys.insert_one.called)
        self.assertIn(self.HEX, key)

    @patch("cypher.storage.db")
    def test_store_error(self, db):
        """
        Сообщение сохраняется в mongodb
        """

        big_msg = b"0" * (1 + app.config["MAX_MSG_LENGTH"])
        db.keys.insert_one().inserted_id = self.HEX

        with self.assertRaises(ValueError):
            key = storage.store(big_msg, expire=10, times=5)

    @patch("cypher.storage.db")
    def test_not_hex_key_error(self, db):
        """
        Ключ не являющийся HEX строкой приводит к ValueError
        """
        with self.assertRaises(ValueError):
            result = storage.restore("not hex key")

    @patch("cypher.storage.db")
    def test_incorrect_key_length_error(self, db):
        """
        Некорректная длина ключа приводит к ValueError
        """
        db.keys.insert_one().inserted_id = self.HEX
        key = storage.store(self.MSG, expire=10, times=5)

        with self.assertRaises(ValueError):
            result = storage.restore(key[2:])

    @patch("cypher.storage.db")
    def test_no_data_in_storage(self, db):
        """
        Отсутствие данных приводит к ValueError
        """
        db.keys.insert_one().inserted_id = self.HEX
        db.keys.find_one_and_update.return_value = None

        key = storage.store(self.MSG, expire=10, times=5)
        with self.assertRaises(ValueError):
            result = storage.restore(key)

    @patch("cypher.storage.db")
    def test_read_from_storage(self, db):
        """
        Зашифровать данные, а затем прочитать их обратно
        """
        db.keys.insert_one().inserted_id = self.HEX
        key = storage.store(self.MSG, expire=10, times=1)

        db.keys.find_one_and_update.return_value = db.keys.insert_one.call_args[0][0]
        result = storage.restore(key)
        self.assertEqual(self.MSG, result[0])
