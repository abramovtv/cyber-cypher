import os

DEBUG = False
# SENTRY_CONFIG = {
#     "dsn": "https://<key>:<secret>@sentry.io/<project>",
#     "release": raven.fetch_git_sha(os.path.dirname(__file__)),
# }

# SERVER_NAME = "127.0.0.1:5000"

BASE_DIR = os.path.dirname(__file__)

STORAGE_KEY_LENGTH = 12
AES_KEY_LENGTH = 16

MAX_EXPIRE_TIME = 5 * 86400  # 5 дней
MAX_TIMES = 15
MAX_TEXT_LENGTH = 64 * 1024  # 64 Kb
MAX_MSG_LENGTH = 5 * 1048576  # 5 Mb

SECRET_KEY = "define-your-secret-key"

BABEL_DEFAULT_LOCALE = "ru_RU"
BABEL_DEFAULT_TIMEZONE = "Europe/Moscow"

MONGO_URL = os.getenv("MONGO_URL", "mongodb://localhost/cypher")
